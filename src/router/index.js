import Vue from 'vue'
import Router from 'vue-router'
import ListArticle from '@/components/ListArticle/ListArticle'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'ListArticle',
      component: ListArticle
    },
    {
      path: '/:id',
      name: 'ListArticle',
      component: ListArticle
    }
  ]
})

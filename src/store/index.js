import Vue from 'vue';
import Vuex from 'vuex';
import Axios from 'axios';

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
      articles:[],
      article:{},
      tags: [],
      statuses:[]
  },
  getters: {
      articles: state => {
          return state.articles;
      },
      article:state => {
          return state.article;
      },
      tags:state => {
          return state.tags;
      },
      tag: state => id =>{
          return state.tags.find(todo => parseInt(todo.id) === id);
      },
      statuses: (state) => {
          return state.statuses;
      },
      status: state => id => {
          return state.statuses.find(todo => parseInt(todo.id) === id);
      },

  },
  mutations: {
    set_articles: (state, data) => {
      state.articles = data;
    },
    set_article: (state, data) => {
      state.article = data;
    },
    set_tags: (state, data) => {
      state.tags = data;
    },
    set_statuses: (state, data) => {
      state.statuses = data;
    },
  },
  actions: {
    get_articles: async (context) => {
      let {data} = await Axios.get('http://127.0.0.1:3000/articles');
      context.commit('set_articles', data);
    },
    get_article: async (context,id) => {
      let {data} = await Axios.get('http://127.0.0.1:3000/articles/'+id);
      context.commit('set_article', data);
    },
    get_tags: async (context) => {
      let {data} = await Axios.get('http://127.0.0.1:3000/tags/');
      context.commit('set_tags', data);
    },
    get_statuses: async (context) => {
      let {data} = await Axios.get('http://127.0.0.1:3000/statuses/');
      context.commit('set_statuses', data);
    }
  }
})
